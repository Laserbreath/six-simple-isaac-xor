/**
    Copyright (c) 2016 Bryn Forrest Benson McKerracher
    All rights reserved.

    Redistribution and use in source and binary forms, with or without modification, are permitted for non-commercial use,
    provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
           disclaimer.

        2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
           disclaimer in the documentation and/or other materials provided in or with the distribution.

        3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
           derived from this software without specific prior written permission.

        4. Any derivative works of the source or binaries provided in or with the distribution are also required to adhere
           to the following condition:

            (i) The source of any derivative work is required to be made easily available to the public in a non-obfuscated
                format.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
    IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
    OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
    OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SIX_H_INCLUDED
#define SIX_H_INCLUDED

#include <string>   //std::string
#include <cmath>    //ceil()
#include <fstream>  //File I/O

#include "Isaac.h"

/*! @file SIX.h
 *  @brief The header file for the SIX engine.
 *  @author Bryn McKerracher.
 *
 *  This file contains the class template and member-function prototypes
 *  for the SIX engine.
*/

/*! @var typedef uint64_t BYTE_GROUP
 *  @brief A type definition for a large integral type.
 *
 *  BYTE_GROUP is used to store arbitrarily large numbers in conjunction
 *  with the GetFileSize() method.
*/
typedef uint64_t BYTE_GROUP;

/*! @var char BYTE
*   @brief A type definition for an object exactly one byte long.
*/
typedef char BYTE;

/*! @var static constexpr unsigned ChunkSize = 2048
*   @brief The number of bytes of data processed by the SIX engine during each cycle.
*
*   @warning Small values of ChunkSize may result in a decrease of performance from
*            the SIX engine.
*
*   ChunkSize denotes the number of bytes the SIX engine will encrypt at a time in a
*   file before moving on to the next chunk.
*/
static constexpr unsigned ChunkSize = 2048;

/*!  @class SIX
 *   @brief The SIX Encryption Engine.
 *
 *   The SIX Engine is an extension of the ISAAC CSPRNG
 *   that produces binary entropy which is then
 *   used for file encryption.
 */

class SIX : public IsaacRand
{
protected:
    /*! @brief Measures a file's size in bytes.
    *
    *  @param [in] filePath The location of the file to be scanned for size.
    *  @return A integral type denoting the number of bytes the specified file contains.
    *
    *  @warning The function will fail if the file contains more than 2^64 bytes of data. However, this is
    *           is nearly 17 million *terabytes* of data, so for practical purposes we can ignore this limit.
    */
    BYTE_GROUP GetFileSize(const std::string& filePath) const;

    /*! @brief Takes a user specified key and extends or reduces it in length (while scrambling it with a series of XOR operations)
    *         until the key is exactly 32 times the size of an 'unsigned' type (This is the maximum amount of data usable in the
    *         IsaacRand::setSeed() function).
    *
    *  @param [in] plainText A plain-text key which is scrambled and processed into an array of 32 unsigned integers to be used to seed the
    *          IsaacRand engine.
    *  @return A const pointer to a array of 32 unsigned integers which is used to seed the IsaacRand engine.
    */
    inline const unsigned* CipherText(std::string& plainText) const;

    /*!  @brief Calculates the number of unsigned integers necessary to store the data contained in a std::string.
     *
     *   @param [in] strData A string containing char data.
     *   @return An unsigned integer denoting the number of unsigned integers necessary to store the amount
     *           of information contained in the string.
     */
    inline unsigned SizeComp(const std::string& strData) const;

    /*!  @brief Encrypts the file specified by filePath.
     *
     *   @param [in] filePath Specifies the file to be encrypted.
     *   @return True if successful, false if not.
     */
    bool revolveTarget(const std::string& filePath);

public:
    /*!  @brief Processes a given plain-text key into an array of unsigned integers and uses them to
     *          to seed the engine.
     *
     *   @param [in][out plainText Plain-text string to be processed and used as an IsaacRand generator seed.
     */
    void setSeed(std::string& plainText);

    /*!  @brief Functor which calls revolveTarget() on the specified file.
     *
     *   @param [in] filePath Specifies the file location to pass to revolveTarget().
     *   @return True if successful, false if not.
     */
    bool operator()(const std::string& filePath);

    /*!  @brief Initialises SIX with a plain-text key to be processed and used to seed the engine.
     *
     *   @param [in] plainText A plain-text string to be processed and used to seed the engine.
     */
    SIX(std::string& plainText);
};

#endif // SIX_H_INCLUDED
