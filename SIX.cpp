/**
    Copyright (c) 2016 Bryn Forrest Benson McKerracher
    All rights reserved.

    Redistribution and use in source and binary forms, with or without modification, are permitted for non-commercial use,
    provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
           disclaimer.

        2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
           disclaimer in the documentation and/or other materials provided in or with the distribution.

        3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
           derived from this software without specific prior written permission.

        4. Any derivative works of the source or binaries provided in or with the distribution are also required to adhere
           to the following condition:

            (i) The source of any derivative work is required to be made easily available to the public in a non-obfuscated
                format.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
    IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
    OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
    OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*/

#include "SIX.h"

/*! @file SIX.cpp
 *  @brief Definitions for the function prototypes provided by SIX.h.
 *  @author Bryn McKerracher.
 *
 *  This file contains the definition for the member-functions for the SIX engine as provided
 *  by SIX.h.
*/

BYTE_GROUP SIX::GetFileSize(const std::string& filePath) const {
    std::ifstream Input(filePath.c_str(), std::ifstream::ate | std::ifstream::binary);
    return Input.tellg();
}

inline const unsigned* SIX::CipherText(std::string& plainText) const {
    //32 is the max number of unsigned seeds IsaacRand seeding will take.
    while (SizeComp(plainText) != 32) {
        static unsigned keyPosition = 0;
        if (SizeComp(plainText) > 32) {
            plainText[plainText.size() - 2] ^= plainText.back();
            plainText[keyPosition++] ^= plainText[plainText.size() - 2];
            plainText.back() = 0;
            plainText.pop_back();
        }
        else {
            plainText += plainText[keyPosition++];
            plainText[plainText.size() - 2] ^= plainText.back();
            plainText[keyPosition] ^= plainText[plainText.size() - 2];
        }
    }
    return (const unsigned*)&plainText[0];
}

inline unsigned SIX::SizeComp(const std::string& strData) const {
    return ceil((double)(strData.size() * (double)sizeof(char)) / (double)sizeof(unsigned));
}

bool SIX::revolveTarget(const std::string& filePath) {
    BYTE_GROUP FileSize = GetFileSize(filePath);
    std::ifstream Input(filePath.c_str(), std::ios::binary);
    if (FileSize > 0 && Input.is_open()) {
        BYTE FileBuffer[ChunkSize];
        BYTE_GROUP const NumberOfChunks = (FileSize % ChunkSize == 0) ? FileSize / ChunkSize : (FileSize / ChunkSize) + 1;
        std::fstream Output(filePath.c_str(), std::ios_base::binary | std::ios_base::out | std::ios_base::in);
        for (unsigned int i = 0; i < NumberOfChunks - 1; ++i) {
            Output.seekp(Input.tellg());
            Input.read(&FileBuffer[0], ChunkSize);
            for (unsigned int k = 0; k < 8; ++k) {
                for (auto& l : FileBuffer)
                    l ^= (getNext() & 1) << k;
            }
            Output.write(&FileBuffer[0], ChunkSize);
        }
        Input.read(&FileBuffer[0], FileSize - (NumberOfChunks - 1) * ChunkSize);
        for (unsigned int k = 0; k < 8; ++k) {
            for (auto& l : FileBuffer)
                l ^= (getNext() & 1) << k;
        }
        Output.write(&FileBuffer[0], FileSize - (NumberOfChunks - 1) * ChunkSize);
        return true;
    }
    return false;
}

void SIX::setSeed(std::string& plainText) {
    IsaacRand::setSeed(CipherText(plainText), 32);
}

bool SIX::operator()(const std::string& filePath) {
    return revolveTarget(filePath);
}

SIX::SIX(std::string& plainText) :
    IsaacRand(CipherText(plainText), 32)
{}

